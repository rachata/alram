#include <Arduino.h>
#include <WiFi.h>
#include <WiFiManager.h>
#include <AsyncTCP.h>
#include "esp_camera.h"
#include "esp_system.h"
#include <ESPAsyncWebServer.h>
#include <EEPROM.h>
hw_timer_t *timer = NULL;
void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}
#include <TridentTD_LineNotify.h>


// Pin definition for CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

const int Led_Flash = 4;
const int trigPin = 12;
const int echoPin = 13;

const int swPin = 12;
bool statusSW = false;

const int relayPin = 16;

boolean startTimer = false;
unsigned long time_now = 0;
int time_capture = 0;
long duration;
int distance;


AsyncWebServer server(80);
int address = 0;
String LINETOKEN = "";
const char* PARAM_1 = "LineToken";
const String inputParam1 = "Line Token";

const char index_html[] PROGMEM = R"rawliteral(
                    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Line Token</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #34a0a4;
        }

        .box {
            width: 70%;
            padding: 40px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #1a759f;
            color: white;
            text-align: center;
            border-radius: 24px;
            box-shadow: 0px 1px 32px 0px rgba(0, 227, 197, 0.59);
        }

        h1 {
            text-transform: uppercase;
            font-weight: 500;
        }

        input::placeholder {
  color: #fff;
}
        input {
            border: 0;
            display: block;
            background: none;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #edf6f9;
            padding: 14px 10px;
            width: 100%;
            outline: none;
            border-radius: 24px;
            color: white;
            font-size: smaller;
            transition: 0.3s;
        }

        input:focus {
            width: 100%;
          
            border-color: #d62828;
        }

        input[type='submit'] {
            border: 0;
            display: block;
            background: none;
            margin: 20px auto;
            text-align: center;
            background-color: #d62828;
            border: 2px solid #d62828;
            padding: 14px 10px;
            width: 45%;
            outline: none;
            border-radius: 24px;
            color: white;
            transition: 0.3s;
            cursor: pointer;
        }

        input[type='submit']:hover {
            background-color: #d62828;
        }
    </style>
</head>

<body>
    <form action="/get" class="box" id="my-form">
        <h1>ตั้งค่า Line</h1>
        <div class="part">
            <input name="LineToken" type="text" placeholder="Line Token">
        </div>
        <input type="submit" value="บันทึก">
    </form>
</body>

</html>
                   )rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

void writeStringToEEPROM(int addrOffset, const String &strToWrite)
{
  byte len = strToWrite.length();
  EEPROM.write(addrOffset, len);
  for (int i = 0; i < len; i++)
  {
    EEPROM.write(addrOffset + 1 + i, strToWrite[i]);
  }
  EEPROM.commit();
}

String readStringFromEEPROM(int addrOffset)
{
  int newStrLen = EEPROM.read(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++)
  {
    data[i] = EEPROM.read(addrOffset + 1 + i);
  }
  data[newStrLen] = '\ 0'; // !!! NOTE !!! Remove the space between the slash "/" and "0" (I've added a space because otherwise there is a display bug)
  return String(data);
}

int i = 0;
void setup() {

  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT);
  pinMode(Led_Flash, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin , LOW);

  WiFi.mode(WIFI_AP_STA);

  WiFiManager wifiManager;

  wifiManager.autoConnect("AlarmSetting");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    i++;
    if (i == 10) {
      wifiManager.resetSettings();
      Serial.print("Reset Success");
      i = 0;
      ESP.restart();
    }
    Serial.print(".");
  }



  digitalWrite(Led_Flash, LOW);

  Serial.printf("\nWiFi connected\nIP : ");
  Serial.println(WiFi.localIP());


  timer = timerBegin(0, 80, true); //timer 0, div 80Mhz
  //  timerAttachInterrupt(timer, &resetModule, true);
  timerAlarmWrite(timer, 20000000, false); //set time in us 15s
  timerAlarmEnable(timer); //enable interrupt

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  if (psramFound()) {
    // FRAMESIZE_ +
    //QQVGA/160x120//QQVGA2/128x160//QCIF/176x144//HQVGA/240x176
    //QVGA/320x240//CIF/400x296//VGA/640x480//SVGA/800x600//XGA/1024x768
    //SXGA/1280x1024//UXGA/1600x1200//QXGA/2048*1536
    config.frame_size = FRAMESIZE_SXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_QQVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  // Init Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  if (!EEPROM.begin(1000)) {
    Serial.println("Failed to initialise EEPROM");
    Serial.println("Restarting...");
    delay(1000);
    ESP.restart();
  }

  String str = EEPROM.readString(address);

  Serial.println("STR LINE");
  Serial.println(str);

  LINETOKEN  = str;
  LINE.setToken(LINETOKEN);

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html);
  });

  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String inputMessage1, inputMessage2, inputMessage3;
    // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
    if (request->getParam(PARAM_1)->value() != "") {
      inputMessage1 = request->getParam(PARAM_1)->value();
    } else {
      inputMessage1 = "none";
    }
    Serial.println(inputParam1 + ": " + inputMessage1);

    String sentence = inputMessage1;
    EEPROM.writeString(address, sentence);

    EEPROM.commit();

    String str = EEPROM.readString(address);

    Serial.println("STR");
    Serial.println(str);

    LINETOKEN  = str;
    LINE.setToken(LINETOKEN);
    request->send(200, "text/html", "<html><head><meta charset='UTF-8'></head><body><h3>ตั้งค่าสำเร็จ</h3><a href='/'>กลับ</a></body></html>");
  });
  server.onNotFound(notFound);
  server.begin();

}
void loop() {

  if (digitalRead(swPin) == 1 && statusSW == false) {
    Camera_capture();
    statusSW = true;
    delay(1000);

  } else if (digitalRead(swPin) == 1 && statusSW == true) {
    Serial.println("N");   statusSW = false;
    digitalWrite(relayPin, LOW);
    delay(1000);

  }
}
void Camera_capture() {
  digitalWrite(Led_Flash, HIGH);
  delay(100);
  digitalWrite(Led_Flash, LOW);
  delay(100);
  digitalWrite(Led_Flash, HIGH);
  camera_fb_t * fb = NULL;
  delay(200);
  // Take Picture with Camera
  fb = esp_camera_fb_get();
  if (!fb) {
    Serial.println("Camera capture failed");
    return;
  }
  digitalWrite(Led_Flash, LOW);
  Send_line(fb->buf, fb->len);
  esp_camera_fb_return(fb);

  digitalWrite(relayPin, HIGH);

}

void Send_line(uint8_t *image_data, size_t   image_size) {
  LINE.notifyPicture("DETECT!!", image_data, image_size);
}
